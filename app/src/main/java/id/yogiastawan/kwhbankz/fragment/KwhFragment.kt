package id.yogiastawan.kwhbankz.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.progressindicator.CircularProgressIndicator
import id.yogiastawan.kwhbankz.R
import id.yogiastawan.kwhbankz.database.DataBaseHelper
import id.yogiastawan.kwhbankz.dialog.EditDialog
import id.yogiastawan.kwhbankz.dialog.EditTenantName
import id.yogiastawan.kwhbankz.listrecycle.DataAdapter
import id.yogiastawan.kwhbankz.listrecycle.DataKwh
import id.yogiastawan.kwhbankz.viewmodel.SharedData
import kotlinx.coroutines.*

class KwhFragment : Fragment() {

    private val TAG = "FRAGMENT_ACTIVITY"
    private lateinit var sharedDataModel: SharedData
    private var dataAdapter: DataAdapter = DataAdapter()
    private var dateId: Int = 0
    private lateinit var recyclerView: RecyclerView
    private lateinit var circularProgressIndicator: CircularProgressIndicator


    companion object {
        fun newInstance(posId: Int): KwhFragment {
            val fragment = KwhFragment()
            val args = Bundle()
            args.putInt("INDEX", posId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onPause() {
        super.onPause()
        Log.d("lifeCycleApp", "fragment ${requireArguments().getInt("INDEX")} on Pause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("lifeCycleApp", "fragment ${requireArguments().getInt("INDEX")} on Stop")
        sharedDataModel.setRecyclerViewState(
            requireArguments().getInt("INDEX"),
            recyclerView.layoutManager?.onSaveInstanceState()!!
        )
    }

    override fun onResume() {
        super.onResume()
        Log.d("lifeCycleApp", "fragment ${requireArguments().getInt("INDEX")} on Resume")
        if (sharedDataModel.getRecyclerViewState(requireArguments().getInt("INDEX")) != null) {
            recyclerView.layoutManager?.onRestoreInstanceState(
                sharedDataModel.getRecyclerViewState(
                    requireArguments().getInt("INDEX")
                )
            )
        }
//        if (recyclerView.adapter?.itemCount == 0) {
//            recyclerView.adapter =
//                sharedDataModel.getDataAdapterVal(requireArguments().getInt("INDEX"))
//            Log.d(
//                "lifeCycleApp",
//                "fragment ${requireArguments().getInt("INDEX")} set adapter from view model"
//            )
//        }
        Log.d(
            "lifeCycleApp",
            "fragment ${requireArguments().getInt("INDEX")} adapter item->${recyclerView.adapter?.itemCount}"
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("lifeCycleApp", "fragment ${requireArguments().getInt("INDEX")} on Destroy")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedDataModel = ViewModelProvider(requireActivity()).get(SharedData::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.fragment_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedDataModel.getDateId().observe(viewLifecycleOwner, {
            dateId = it
        })

        //view config
        recyclerView = view.findViewById(R.id.recyclerview)
        val divider = DividerItemDecoration(
            recyclerView.context,
            LinearLayoutManager.VERTICAL
        )
        divider.setDrawable(
            AppCompatResources.getDrawable(
                requireContext(),
                R.drawable.recyclerview_item_divider
            )!!
        )
        circularProgressIndicator = view.findViewById(R.id.circularProgress)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
//        recyclerView.adapter = dataAdapter
        recyclerView.addItemDecoration(divider)
        recyclerView.visibility = View.GONE
        sharedDataModel.getDataAdapter(requireArguments().getInt("INDEX"))
            .observe(requireActivity(), {
                CoroutineScope(Dispatchers.Main).launch {
                    if (circularProgressIndicator.visibility == View.GONE) {
                        circularProgressIndicator.visibility = View.VISIBLE
                    }
                    it.setOnItemClicked { data, index ->
                        if (data.kwhValueId == null) {
                            showEditDialog(resources.getString(R.string.edit_data), data, index)
                        }

                    }
                    it.setOnLongItemClicked { dataKwh, i ->
                        showPopUpMenu(dataKwh, i)
                    }
                    recyclerView.adapter = it
                    delay(300)
                    Log.d(
                        "lifeCycleApp",
                        "fragment ${requireArguments().getInt("INDEX")} get adapter from view model= ${recyclerView.adapter?.itemCount}"
                    )
                    if (recyclerView.visibility == View.GONE) {
                        recyclerView.visibility = View.VISIBLE
                    }
                    circularProgressIndicator.visibility = View.GONE
                }
            })
    }

    private fun showEditDialog(title: String, dataKwh: DataKwh, posItem: Int) {
        val dialog =
            EditDialog.newInstance(title, dataKwh, requireArguments().getInt("INDEX"), posItem)
        dialog.show(childFragmentManager, EditDialog.EDIT_DIALOG_TAG)
    }

    private fun showUpdateTenant(title: String, dataKwh: DataKwh, posItem: Int) {
        val dialog =
            EditTenantName.newInstance(title, dataKwh, requireArguments().getInt("INDEX"), posItem)
        dialog.show(childFragmentManager, EditTenantName.EDIT_TENANT_DIALOG_TAG)
    }

    private fun showPopUpMenu(dataKwh: DataKwh, posItem: Int) {
        val rel: RecyclerView = this.requireView().findViewById(R.id.recyclerview)
        val dBHelper = DataBaseHelper(requireContext())
        val popupMenu = PopupMenu(
            requireContext(),
            rel.findViewHolderForAdapterPosition(posItem)!!.itemView,
            Gravity.FILL_HORIZONTAL
        )
        popupMenu.inflate(R.menu.item_popup_menu)
        if (dataKwh.kwhValueId == null) {
            val menu = popupMenu.menu
            menu.findItem(R.id.menu_item_update_data).title =
                resources.getString(R.string.insert_value)
        }
        popupMenu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menu_item_update_data -> {
                    if (dataKwh.kwhValueId == null) {
                        //insert kwh
                        showEditDialog(resources.getString(R.string.edit_data), dataKwh, posItem)
                    } else {
//                        update kwh
                        showEditDialog(resources.getString(R.string.update_data), dataKwh, posItem)
                    }
                    true
                }
                R.id.menu_item_edit_tenant -> {
                    showUpdateTenant(resources.getString(R.string.edit_tenant), dataKwh, posItem)
                    true
                }
                R.id.menu_item_delete_tenant -> {
                    CoroutineScope(Dispatchers.Main).launch {
                        val isDeleted = async(Dispatchers.IO) {
                            dBHelper.deleteTenantAndAssociation(
                                dataKwh.tenantId!!,
                                requireArguments().getInt("INDEX")
                            )
                        }
                        if (isDeleted.await()) {
                            sharedDataModel.deleteDataItem(
                                requireArguments().getInt("INDEX"),
                                posItem
                            )
                        }
                    }
                    true
                }
                else -> {
                    popupMenu.dismiss()
                    false
                }
            }
        }
        popupMenu.show()
    }
}
package id.yogiastawan.kwhbankz.viewmodel

import android.os.Parcelable
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.tabs.TabLayoutMediator
import id.yogiastawan.kwhbankz.fragment.CollectionFragmentAdapter
import id.yogiastawan.kwhbankz.listrecycle.DataAdapter
import id.yogiastawan.kwhbankz.listrecycle.DataKwh

class SharedData : ViewModel() {

    private var _data = mutableListOf<MutableLiveData<DataAdapter>>()
    private var _dateID = MutableLiveData<Int>()

    private var recyclerViewState= mutableListOf<Parcelable?>()


    init {
        for (i: Int in 0 until 7) {
            _data.add(MutableLiveData(DataAdapter()))
            recyclerViewState.add(null)
        }
    }

    fun setRecyclerViewState(indexPosition: Int, state:Parcelable){
        recyclerViewState[indexPosition]=state
    }

    fun getRecyclerViewState(indexPosition: Int):Parcelable?{
        return recyclerViewState[indexPosition]
    }

    fun getDateId(): LiveData<Int> {
        return _dateID
    }

    fun setDateId(dateId: Int) {
        _dateID.value = dateId
        Log.d("ViewModel", "id_date: ${_dateID.value}")
    }

    fun getDataAdapter(indexPosition: Int): LiveData<DataAdapter> {
        return _data[indexPosition]
    }

    fun getDataAdapterVal(indexPosition: Int):DataAdapter?{
        return _data[indexPosition].value
    }

    fun getDataAdapterContentList(indexPosition: Int):List<DataKwh>{
        return _data[indexPosition].value!!.getDataList()
    }

    fun setDataList(indexPosition: Int, dataKwhList: MutableList<DataKwh>) {
        Log.d("ViewModel:", "${_data.size}")
        if (indexPosition >= _data.size) {
            _data.add(indexPosition, MutableLiveData(DataAdapter()))
        }
        _data[indexPosition].value!!.setListItem(dataKwhList)
    }

    fun deleteDataItem(indexPosition: Int, positionItem: Int) {
        _data[indexPosition].value!!.deleteItem(positionItem)
    }

    fun addDataList(indexPosition: Int, dataKwhList: MutableList<DataKwh>) {
        if (indexPosition >= _data.size) {
            _data.add(indexPosition, MutableLiveData(DataAdapter()))
        }
        _data[indexPosition].value!!.addListItem(dataKwhList)
    }

    fun addDataItem(indexPosition: Int, dataKwh: DataKwh) {
        _data[indexPosition].value!!.addData(dataKwh)
        Log.d("ViewModel", "SizeItem: ${_data[indexPosition].value!!.itemCount}")
    }

    fun insertDataItem(indexPosition: Int, dataKwh: DataKwh,positionItem: Int){
        _data[indexPosition].value!!.insertItem(positionItem,dataKwh)
    }

    fun editDataValueItem(indexPosition: Int, kwh: Float, kwhId:Int, positionItem: Int) {
        val dataKwh = _data[indexPosition].value!!.getDataItemAt(positionItem)
        dataKwh.kwh = kwh
        dataKwh.kwhValueId=kwhId
        _data[indexPosition].value!!.updateItem(dataKwh,positionItem)
    }
    fun editDataNameItem(indexPosition: Int, name: String, positionItem: Int) {
        val dataKwh = _data[indexPosition].value!!.getDataItemAt(positionItem)
        dataKwh.name = name
        _data[indexPosition].value!!.updateItem(dataKwh,positionItem)
        Log.d("ViewModel","Edit name at $positionItem to $name")
    }

    fun clearDataList(indexPosition: Int){
        _data[indexPosition].value!!.clearAllItem()
    }

}
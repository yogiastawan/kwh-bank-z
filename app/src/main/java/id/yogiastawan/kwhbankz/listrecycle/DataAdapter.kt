package id.yogiastawan.kwhbankz.listrecycle

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.yogiastawan.kwhbankz.R

class DataAdapter() : RecyclerView.Adapter<DataHolder>() {
    private var dataList = mutableListOf<DataKwh>()
    private var onItemClicked: ((DataKwh, Int) -> Unit)? = null
    private var onLongItemClicked: ((DataKwh, Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataHolder {
        val v: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_recyclerview, parent, false)
        return DataHolder(v, onItemClicked = {
            onItemClicked?.invoke(dataList[it], it)
        }, onLongItemClicked = {
            onLongItemClicked?.invoke(dataList[it], it)
        })
    }

    override fun onBindViewHolder(holder: DataHolder, position: Int) {
        holder.bindToView(dataList[position], position)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun getDataItemAt(position: Int): DataKwh {
        return dataList[position]
    }

    fun getDataList():List<DataKwh>{
        return dataList
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListItem(data: MutableList<DataKwh>) {
        dataList.clear()
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addListItem(data: MutableList<DataKwh>) {
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun addData(data: DataKwh) {
        dataList.add(data)
        Log.d("DataADAPTER", "sizeofList: ${dataList.size}")
        notifyItemInserted(dataList.size - 1)
    }

    fun insertItem(position: Int, data: DataKwh) {
        dataList.add(position, data)
        Log.d("DataADAPTER", "sizeofList: ${dataList.size}")
        notifyItemInserted(position)
    }

    fun deleteItem(position: Int) {
        dataList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun updateItem(data: DataKwh, position: Int) {
        dataList[position] = data
        notifyItemChanged(position)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clearAllItem() {
        dataList.clear()
        notifyDataSetChanged()
    }

    fun moveItem(fromPosition: Int, toPosition: Int) {
        val data = dataList[fromPosition]
        dataList.removeAt(fromPosition)
        dataList.add(toPosition, data)
        notifyItemMoved(fromPosition, toPosition)

    }

    fun setOnItemClicked(data: ((DataKwh, Int) -> Unit)?) {
        onItemClicked = data
    }

    fun setOnLongItemClicked(data: ((DataKwh, Int) -> Unit)?) {
        onLongItemClicked = data
    }
}
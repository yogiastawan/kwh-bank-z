package id.yogiastawan.kwhbankz.listrecycle

import android.os.Build
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import id.yogiastawan.kwhbankz.R
import java.util.*

class DataHolder(
    itemView: View,
    onItemClicked: (Int) -> Unit,
    onLongItemClicked: (Int) -> Unit
) : RecyclerView.ViewHolder(itemView) {

    private val tenantName: AppCompatTextView = itemView.findViewById(R.id.kwh_name)
    private val kwhValue: AppCompatTextView = itemView.findViewById(R.id.kwh_value)
    private val materialCardView:MaterialCardView=itemView.findViewById(R.id.item_card_view)
    private var pos: Int? = null

    init {
        itemView.setOnClickListener {
            onItemClicked(adapterPosition)
        }
        itemView.setOnLongClickListener {
            onLongItemClicked(adapterPosition)
            true
        }
    }

    fun bindToView(dataKwh: DataKwh, pos: Int) {
        tenantName.text = dataKwh.name
        if (dataKwh.kwhValueId != null) {
            kwhValue.text = String.format(Locale.getDefault(), "%.2f", dataKwh.kwh)
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                    materialCardView.strokeColor= itemView.resources.getColor(R.color.good_color, itemView.context.theme)
                }
                else -> {
                    materialCardView.strokeColor=itemView.resources.getColor(R.color.good_color)
                }
            }
        }else{
            if (dataKwh.isActive){
                when {
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                        materialCardView.strokeColor= itemView.resources.getColor(R.color.error_color, itemView.context.theme)
                    }
                    else -> {
                        materialCardView.strokeColor=itemView.resources.getColor(R.color.error_color)
                    }
                }
            }else{
                when {
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                        materialCardView.strokeColor= itemView.resources.getColor(R.color.default_color, itemView.context.theme)
                    }
                    else -> {
                        materialCardView.strokeColor=itemView.resources.getColor(R.color.default_color)
                    }
                }
            }
        }
        this.pos = pos
    }

}
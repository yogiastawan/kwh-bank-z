package id.yogiastawan.kwhbankz.listrecycle

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DataKwh(
    var tenantId: Int? = null,
    var name: String? = null,
    var kwhValueId: Int? = null,
    var kwh: Float? = null,
    val isActive:Boolean=false
):Parcelable
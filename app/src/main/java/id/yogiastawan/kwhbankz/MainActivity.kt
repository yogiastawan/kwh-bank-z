package id.yogiastawan.kwhbankz

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.DecelerateInterpolator
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import id.yogiastawan.kwhbankz.database.DataBaseHelper
import id.yogiastawan.kwhbankz.database.DateRecorded
import id.yogiastawan.kwhbankz.fragment.CollectionFragmentAdapter
import id.yogiastawan.kwhbankz.listrecycle.DataKwh
import id.yogiastawan.kwhbankz.odscreator.*
import id.yogiastawan.kwhbankz.odscreator.celltype.CalCextType
import id.yogiastawan.kwhbankz.odscreator.celltype.OfficeType
import id.yogiastawan.kwhbankz.odscreator.fontface.StyleFont
import id.yogiastawan.kwhbankz.odscreator.stylefamily.*
import id.yogiastawan.kwhbankz.viewmodel.SharedData
import kotlinx.coroutines.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private val regActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                CoroutineScope(Dispatchers.Main).launch {
                    writeODSFile(it.data!!.data)
                }
            }
        }


    private val TAG = "MainActivity"
    private var isFABMenuOpen = false

    private lateinit var fabParent: FloatingActionButton
    private lateinit var fabNewTask: FloatingActionButton
    private lateinit var fabNewTenant: FloatingActionButton
    private lateinit var fabShare: FloatingActionButton

    private lateinit var txtViewNewTask: AppCompatTextView
    private lateinit var txtViewNewTenant: AppCompatTextView
    private lateinit var txtViewShare: AppCompatTextView

    //    private lateinit var fabCloseAnim: Animation
    private lateinit var rotate360Clock: Animation
    private lateinit var rotate360AntiClock: Animation
    private lateinit var fabRotateClockAnim: Animation
    private lateinit var fabRotateAntiClockAnim: Animation
    private lateinit var tvFadeInAnim: Animation
    private lateinit var tvFadeOutAnim: Animation
    private lateinit var sharedDataModel: SharedData
    private lateinit var dBHelper: DataBaseHelper

    private lateinit var progressDialog: AlertDialog

    override fun onResume() {
        super.onResume()
        Log.d("lifeCycleApp", "on Resume")
    }

    override fun onStop() {
        super.onStop()
        Log.d("lifeCycleApp", "on Stop")
    }

    override fun onPause() {
        super.onPause()
        Log.d("lifeCycleApp", "on Pause")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("lifeCycleApp", "on Destroy")

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dBHelper = DataBaseHelper(this)
        sharedDataModel = ViewModelProvider(this).get(SharedData::class.java)
//        CoroutineScope(Dispatchers.Main).launch {
//            insertData()
//        }
        //remove actionbar elevation
        supportActionBar!!.elevation = 0f


        //get viewpager2
        val viewPager = findViewById<ViewPager2>(R.id.viewpager)
        //set viewpager adapter

        viewPager.adapter = CollectionFragmentAdapter(this)
        //set tab layout to viewPager
        val tabLayout = findViewById<TabLayout>(R.id.tab_layout)
        val tabLayoutMediator =
            TabLayoutMediator(tabLayout, viewPager, false, true) { tab, position ->
                when (position) {
                    0 -> {
                        tab.setIcon(R.drawable.ic_gf)
                        tab.setText(R.string.ground_floor)
                    }
                    1 -> {
                        tab.setIcon(R.drawable.ic_f1)
                        tab.setText(R.string.floor_1)
                    }
                    2 -> {
                        tab.setIcon(R.drawable.ic_ghost)
                        tab.setText(R.string.trans_studio)
                    }
                    3 -> {
                        tab.setIcon(R.drawable.ic_mart)
                        tab.setText(R.string.trans_mart)
                    }
                    4 -> {
                        tab.setIcon(R.drawable.ic_store)
                        tab.setText(R.string.store)
                    }
                    5 -> {
                        tab.setIcon(R.drawable.ic_server)
                        tab.setText(R.string.server)
                    }
                    6 -> {
                        tab.setIcon(R.drawable.ic_public)
                        tab.setText(R.string.public_tag)
                    }
                }
            }
        tabLayoutMediator.attach()


        //fab menu
        fabParent = findViewById(R.id.fab_parent)
        fabNewTask = findViewById(R.id.fab_add_new_task)
        fabNewTenant = findViewById(R.id.fab_add_new_tenant)
        fabShare = findViewById(R.id.fab_share)

        txtViewNewTask = findViewById(R.id.txt_view_new_task)
        txtViewNewTask.alpha = 0f
        txtViewNewTenant = findViewById(R.id.txt_view_new_tenant)
        txtViewNewTenant.alpha = 0f
        txtViewShare = findViewById(R.id.txt_share)
        txtViewShare.alpha = 0f
//        fab anim
//        fabCloseAnim = AnimationUtils.loadAnimation(this, R.anim.anim_fab_hide)
        rotate360Clock = AnimationUtils.loadAnimation(this, R.anim.anim_rotate_clock_360)
        rotate360AntiClock = AnimationUtils.loadAnimation(this, R.anim.anim_rotate_anticlock_360)
        fabRotateClockAnim = AnimationUtils.loadAnimation(this, R.anim.anim_rotate_clock)
        fabRotateAntiClockAnim = AnimationUtils.loadAnimation(this, R.anim.anim_rotate_anticlock)
        tvFadeInAnim = AnimationUtils.loadAnimation(this, R.anim.anim_fade_in)
        tvFadeOutAnim = AnimationUtils.loadAnimation(this, R.anim.anim_fade_out)
        fabParent.setOnClickListener {
            if (isFABMenuOpen) {
                closeFabMenu()
            } else {
                showFabMenu()
            }
        }
        fabNewTask.setOnClickListener {
            //add new date to db Table Date and
            val date = DateRecorded(Date())
            CoroutineScope(Dispatchers.Main).launch {
                val iDDate = async(Dispatchers.IO) {//get the Id Date
                    dBHelper.insertDate(date)
                }
                if (iDDate.await() > 0) {
                    sharedDataModel.setDateId(iDDate.await().toInt())//update date in view model
                    //get all tenant and add view model adapter
                }
                //close fab menu
                closeFabMenu()
            }
        }
        var a = 0
        fabNewTenant.setOnClickListener {
            //add new tenant to db table tenant and get the Id
            //insert to list live data
            //close fab menu
            val newName = "Inserted tenant ${a++}"
            CoroutineScope(Dispatchers.Main).launch {
                val tenantId = async(Dispatchers.IO) {
                    dBHelper.insertTenantNameWithString(newName, viewPager.currentItem)
                }
                if (tenantId.await() > 0) {
//                    sharedDataModel.editDataNameItem(viewPager.currentItem,)
                    sharedDataModel.addDataItem(
                        viewPager.currentItem,
                        DataKwh(tenantId.await().toInt(), newName, null, null)
                    )
                }
            }
            closeFabMenu()
        }

        fabShare.setOnClickListener {
//            dataShared.deleteDataItem(viewPager.currentItem,0)
            //create csv file
//            val metaXml = MetaXML("yogi astawan")
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
//                    metaXml.writeMetaXMLFile(Environment.getStorageDirectory())
                }
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {

                }
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                    //add permission
//                    metaXml.writeMetaXMLFile(Environment.getExternalStorageDirectory())
                }

            }

            //get date as name
            val dateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            val name = dateFormat.format(Date())

            val filePath = File(applicationContext.getExternalFilesDir(null), "ODS")
            if (writeODSFileTemp(name)) {
                val fileToSend = File(filePath, "$name${File.separator}[$name]-log sheet.ods")
                val uriFile = FileProvider.getUriForFile(
                    applicationContext,
                    "id.yogiastawan.kwhbankz.data",
                    fileToSend
                )
//                Log.d(TAG, "$uriFile")
                //broadcast share
//                use this if want to show use once or always menu
//                val shareIntent = ShareCompat.IntentBuilder(applicationContext)
//                shareIntent.setStream(uriFile)
//                shareIntent.setChooserTitle(getString(R.string.share))
//                shareIntent.createChooserIntent()
//                shareIntent.setType("application/vnd.oasis.opendocument.spreadsheet")
//                shareIntent.intent.flags=Intent.FLAG_GRANT_READ_URI_PERMISSION
//                shareIntent.intent.action=Intent.ACTION_SEND
//                startActivity(shareIntent.intent)
                val intent = Intent(Intent.ACTION_SEND).apply {
                    flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
//                    clipData=ClipData.newRawUri("Hlalala",uriFile)//not work
                    type = "application/vnd.oasis.opendocument.spreadsheet"
                }
                intent.putExtra(Intent.EXTRA_STREAM, uriFile)
                startActivity(Intent.createChooser(intent, getString(R.string.share)))
            }
            //close fab menu
            closeFabMenu()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_item_save) {
//            when {
//                Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
////                    metaXml.writeMetaXMLFile(Environment.getStorageDirectory())
//                }
//                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {
//
//                }
//                Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
//                    //add permission
////                    metaXml.writeMetaXMLFile(Environment.getExternalStorageDirectory())
//                }
//
//            }
//            val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
//                addCategory(Intent.CATEGORY_OPENABLE)
//                type = "application/vnd.oasis.opendocument.spreadsheet"
//                putExtra(Intent.EXTRA_TITLE, "Test.ods")
//            }
//            regActivityResult.launch(intent)
            CoroutineScope(Dispatchers.Main).launch {
                insertData()
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showFabMenu() {
        fabParent.startAnimation(fabRotateClockAnim)
        fabNewTask.startAnimation(rotate360Clock)
        fabNewTenant.startAnimation(rotate360Clock)
        fabShare.startAnimation(rotate360Clock)
        txtViewNewTask.startAnimation(tvFadeInAnim)
        txtViewNewTenant.startAnimation(tvFadeInAnim)
        txtViewShare.startAnimation(tvFadeInAnim)
        val transition = ChangeBounds()
        transition.duration = 500
        transition.interpolator = DecelerateInterpolator()
        val constraintLayout = findViewById<ConstraintLayout>(R.id.fab_menu_closed)
        val constraintSet = ConstraintSet()
        constraintSet.clone(this, R.layout.fab_view_show)
        TransitionManager.beginDelayedTransition(constraintLayout, transition)
        constraintSet.applyTo(constraintLayout)
        isFABMenuOpen = true
    }

    private fun closeFabMenu() {
        fabParent.startAnimation(fabRotateAntiClockAnim)
        fabNewTask.startAnimation(rotate360AntiClock)
        fabNewTenant.startAnimation(rotate360AntiClock)
        fabShare.startAnimation(rotate360AntiClock)
        txtViewNewTenant.startAnimation(tvFadeOutAnim)
        txtViewNewTask.startAnimation(tvFadeOutAnim)
        txtViewShare.startAnimation(tvFadeOutAnim)
        val transition = ChangeBounds()
        transition.duration = 500
        transition.interpolator = DecelerateInterpolator()
        val constraintLayout = findViewById<ConstraintLayout>(R.id.fab_menu_closed)
        val constraintSet = ConstraintSet()
        constraintSet.clone(this, R.layout.fab_view)
        TransitionManager.beginDelayedTransition(constraintLayout, transition)
        constraintSet.applyTo(constraintLayout)

        isFABMenuOpen = false
    }

    private suspend fun getDataOnStart() {
        withContext(Dispatchers.Main) {

            //get list of Date
            //if list date>0 do:
            //add date to dropdown
            //get list tenant Id  and tenant name in table tenant
            //get of  kwh id, kwh, status where date id = tagged date id AND tenant id= tagged tenant id
            //insert to view model adapter
        }
    }

    private suspend fun insertData() {
        for (i: Int in 0 until 7) {
            val kwhList = mutableListOf<DataKwh>()
            sharedDataModel.setDateId(i)
            withContext(Dispatchers.Default) {
                for (j: Int in 0 until (i * 30) + 4) {
                    kwhList.add(
                        DataKwh(
                            j,
                            "Tenant $j",
                            if (i == 0) null else j,
                            if (i == 0) null else (i * (j + 10)).toFloat(),
                            j != 0
                        )
                    )
//                sharedDataModel.addDataItem(i,DataKwh(j, "Tenant $j", j, (i * j + 120).toFloat()))
                }
            }
            sharedDataModel.addDataList(i, kwhList)
            Log.d("MAINACTIVITY", "insert data in coroutine")
        }

    }

    private suspend fun writeODSFile(uri: Uri?) {
        dialogProgress()
        withContext(Dispatchers.Main) {
            if (uri != null) {
                progressDialog.setMessage("creating component")
//                Log.d(TAG, uri.toFile().name)
                val isMetaSuccess = async(Dispatchers.IO) {
                    val metaXML = MetaXML("KwhBankZet", applicationContext)
                    metaXML.writeMetaXMLFile(File("${uri.path}").name)
                }
                val isManiSuccess = async(Dispatchers.IO) {
                    val manifestXML = ManifestXML(applicationContext)
                    manifestXML.writeManifestXML(File("${uri.path}").name)
                }
                val isContentSuccess = async(Dispatchers.IO) {
                    val contentXML = ContentXML(applicationContext)
                    createContentXML(contentXML)
                    contentXML.writeContentXMLFile(File("${uri.path}").name)
                }
                val isRDFSuccess = async(Dispatchers.IO) {
                    val manifestRDF = ManifestRDF(applicationContext)
                    manifestRDF.writeManifestRDFFile(File("${uri.path}").name)
                }
                if (isManiSuccess.await() && isMetaSuccess.await() && isContentSuccess.await() && isRDFSuccess.await()) {
                    progressDialog.setMessage("Compress file to ODS")
                    withContext(Dispatchers.IO) {
                        val odsCreator = ODSCreator(applicationContext)
                        odsCreator.createODS(uri)
                    }
                    progressDialog.setMessage("Removing tmp file")
                    progressDialog.dismiss()
                    //delete tmp file
                    applicationContext.getExternalFilesDir("ODS${File.separator}${File("${uri.path}").name}")
                        ?.deleteRecursively()
                } else {
                    progressDialog.setMessage("Error")
                }
            }
        }
    }

    private fun dialogProgress() {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.apply {
            title = "Saving file"
            setMessage("Creating file")
            setNegativeButton("Cancel") { _, _ ->

            }
            setCancelable(false)
        }

        progressDialog = alertDialogBuilder.create()
        progressDialog.show()
    }

    private fun createContentXML(contentXML: ContentXML) {
        //createStyle
        val fontFace =
            FontFace().apply {
                addStyleFont(StyleFont("Liberation Sans", "swiss"))
                addStyleFont(StyleFont("Lohit Devanagari", "system"))
                addStyleFont(StyleFont("Noto Sans CJK SC", "system"))
            }
        val co1: Style = Style("co1", StyleFamily.TABLE_COLUMN).apply {
            setStyleTableColumn(StyleTableColumn(0.889f))
        }
        val ro1 = Style("ro1", StyleFamily.TABLE_ROW).apply {
            setStyleTableRow(StyleTableRow(0.178f, true))
        }
        val ta1 = Style("ta1", StyleFamily.TABLE).apply {
            setStyleTable(StyleTable(true, WritingMode.LR_TB))
        }
        val officeStyle = OfficeStyle().apply {
            addStyle(co1)
            addStyle(ro1)
            addStyle(ta1)
        }

        val spreedSheet = SpreedSheet()

        val tenantTable = Table("Tenant", ta1).apply {
            addTableColumn(TableColumn(co1, 2))
            for (i: Int in 0 until 50) {
                val tableRow = TableRow(ro1)
                val tableCell =
                    TableCell(false, OfficeType.STRING, CalCextType.STRING, null, null, null, null)
                tableCell.setTextTag(TextTag("Tenant $i"))
                tableRow.addTableCell(tableCell)
                val tableCell2 =
                    TableCell(
                        false,
                        OfficeType.FLOAT,
                        CalCextType.FLOAT,
                        (i * 100f / 6f).toFloat(),
                        null,
                        null,
                        null
                    )
                tableCell2.setTextTag(TextTag("${i * 100f / 6f}"))
                tableRow.addTableCell(tableCell2)
                addTableRow(tableRow)
            }
        }
        spreedSheet.addTable(tenantTable)
        val publicTable = Table("Public", ta1).apply {
            addTableColumn(TableColumn(co1, 2))
            for (i: Int in 0 until 50) {
                val tableRow = TableRow(ro1)
                val tableCell =
                    TableCell(false, OfficeType.STRING, CalCextType.STRING, null, null, null, null)
                tableCell.setTextTag(TextTag("Tenant $i"))
                tableRow.addTableCell(tableCell)
                val tableCell2 =
                    TableCell(
                        false,
                        OfficeType.FLOAT,
                        CalCextType.FLOAT,
                        (i * 100f / 6f).toFloat(),
                        null,
                        null,
                        null
                    )
                tableCell2.setTextTag(TextTag("${i * 100f / 6f}"))
                tableRow.addTableCell(tableCell2)
                addTableRow(tableRow)
            }
        }
        spreedSheet.addTable(publicTable)

        val officeBody = OfficeBody(spreedSheet)

        contentXML.setFontFace(fontFace)
        contentXML.setOfficeStyle(officeStyle)
        contentXML.setOfficeBody(officeBody)

    }

    private fun writeODSFileTemp(name: String): Boolean {
        dialogProgress()

        progressDialog.setMessage("creating component")
        val metaXML = MetaXML("KwhBankZet", applicationContext)
        val isMetaSuccess = metaXML.writeMetaXMLFile(name)
        val manifestXML = ManifestXML(applicationContext)
        val isManiSuccess = manifestXML.writeManifestXML(name)

        val contentXML = ContentXML(applicationContext)
        createContentXML(contentXML)
        val isContentSuccess = contentXML.writeContentXMLFile(name)

        val manifestRDF = ManifestRDF(applicationContext)
        val isRDFSuccess =
            manifestRDF.writeManifestRDFFile(name)
        if (isManiSuccess && isMetaSuccess && isContentSuccess && isRDFSuccess) {
            progressDialog.setMessage("Compress file to ODS")
            val odsCreator = ODSCreator(applicationContext)
            odsCreator.createODSTemp(name)
            progressDialog.dismiss()
        } else {
            progressDialog.setMessage("Error")
            return false
        }
        return true
    }
}

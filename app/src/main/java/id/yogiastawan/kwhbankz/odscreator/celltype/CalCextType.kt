package id.yogiastawan.kwhbankz.odscreator.celltype

enum class CalCextType(val type:String) {
    STRING("string"),
    FLOAT("float"),
}
package id.yogiastawan.kwhbankz.odscreator.tag

import id.yogiastawan.kwhbankz.odscreator.ODSNameSpaceXML

open class Tag(private var tagName: String) {

    val tags = mutableListOf<Tag>()

    var value: String = ""

    private var tagProperty = ""
    private var tagNameSpace = ""

    open fun addChildTag(tag: Tag) {
        this.tags.add(tag)
    }

    open fun getTagsChild(): List<Tag> {
        return tags
    }

    open fun getTagString(): String {
        var content = "<$tagName$tagNameSpace$tagProperty"
        var tagEnd = "/>"
        if (tags.size > 0 || value!!.isNotEmpty()) {
            content += ">"
            tagEnd = "</$tagName>"
        }
        content += value
        for (i in tags) {
            content += i.getTagString()
        }
        return content + tagEnd
    }

    fun addPropertyTag(propertyName: String, propertyValue: String) {
        tagProperty += " $propertyName=\"$propertyValue\""
    }

    fun addXMLNAmeSpace(nameSpace: ODSNameSpaceXML) {
        tagNameSpace += " ${nameSpace.namSpace}"
    }

    fun setTagName(tagName: String) {
        this.tagName = tagName
    }

    fun getTagName(): String {
        return tagName
    }

    fun setValueTag(value: String) {
        this.value = value
    }

    fun getValueTag(): String {
        return value
    }

    open fun setChildTag(tag: Tag) {
        tags.clear()
        tags.add(tag)
    }

    fun addMultipleChildTag(listTag: List<Tag>) {
        tags.addAll(listTag)
    }

}
package id.yogiastawan.kwhbankz.odscreator

import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class OfficeBody(private var spreedSheet: SpreedSheet) : Tag("office:body") {

    override fun getTagString(): String {
        setChildTag(spreedSheet)
        return super.getTagString()
    }

    fun setSpreedSheet(spreedSheet: SpreedSheet) {
        this.spreedSheet = spreedSheet
    }

    fun getSpreedSheet(): SpreedSheet {
        return spreedSheet
    }

    @Deprecated(
        "Cannot insert child tag in TextTag", level = DeprecationLevel.HIDDEN
    )
    override fun addChildTag(tag: Tag) {
        super.addChildTag(tag)
    }
}
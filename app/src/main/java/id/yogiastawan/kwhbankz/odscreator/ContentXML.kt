package id.yogiastawan.kwhbankz.odscreator

import android.content.Context
import android.util.Log
import id.yogiastawan.kwhbankz.odscreator.tag.Tag
import java.io.File
import java.io.FileOutputStream

class ContentXML(val context: Context) {
    private val tag = "office:document-content"

    private lateinit var fontFace: FontFace

    fun setFontFace(fontFace: FontFace) {
        this.fontFace = fontFace
    }

    private lateinit var officeStyle: OfficeStyle
    fun setOfficeStyle(officeStyle: OfficeStyle) {
        this.officeStyle = officeStyle
    }

    private lateinit var officeBody: OfficeBody
    fun setOfficeBody(officeBody: OfficeBody) {
        this.officeBody = officeBody
    }


    fun getContentXMLString(): String {
        val tag = Tag("office:document-content").apply {

            addXMLNAmeSpace(ODSNameSpaceXML.PRESENTATION)
            addXMLNAmeSpace(ODSNameSpaceXML.CSS3T)
            addXMLNAmeSpace(ODSNameSpaceXML.GRDDL)
            addXMLNAmeSpace(ODSNameSpaceXML.XHTML)
            addXMLNAmeSpace(ODSNameSpaceXML.FORMX)
            addXMLNAmeSpace(ODSNameSpaceXML.XSI)
            addXMLNAmeSpace(ODSNameSpaceXML.RPT)
            addXMLNAmeSpace(ODSNameSpaceXML.DC)
            addXMLNAmeSpace(ODSNameSpaceXML.CHART)
            addXMLNAmeSpace(ODSNameSpaceXML.SVG)
            addXMLNAmeSpace(ODSNameSpaceXML.DRAW)
            addXMLNAmeSpace(ODSNameSpaceXML.TEXT)
            addXMLNAmeSpace(ODSNameSpaceXML.OOOC)
            addXMLNAmeSpace(ODSNameSpaceXML.STYLE)
            addXMLNAmeSpace(ODSNameSpaceXML.OOOW)
            addXMLNAmeSpace(ODSNameSpaceXML.META)
            addXMLNAmeSpace(ODSNameSpaceXML.XLINK)
            addXMLNAmeSpace(ODSNameSpaceXML.FO)
            addXMLNAmeSpace(ODSNameSpaceXML.OOO)
            addXMLNAmeSpace(ODSNameSpaceXML.OFFICE)
            addXMLNAmeSpace(ODSNameSpaceXML.DR3D)
            addXMLNAmeSpace(ODSNameSpaceXML.TABLE)
            addXMLNAmeSpace(ODSNameSpaceXML.NUMBER)
            addXMLNAmeSpace(ODSNameSpaceXML.OF)
            addXMLNAmeSpace(ODSNameSpaceXML.CALCEXT)
            addXMLNAmeSpace(ODSNameSpaceXML.TABLEOOO)
            addXMLNAmeSpace(ODSNameSpaceXML.DRAWOOO)
            addXMLNAmeSpace(ODSNameSpaceXML.LOEXT)
            addXMLNAmeSpace(ODSNameSpaceXML.DOM)
            addXMLNAmeSpace(ODSNameSpaceXML.FIELD)
            addXMLNAmeSpace(ODSNameSpaceXML.XSD)
            addXMLNAmeSpace(ODSNameSpaceXML.MATH)
            addXMLNAmeSpace(ODSNameSpaceXML.FORM)
            addXMLNAmeSpace(ODSNameSpaceXML.SCRIPT)
            addXMLNAmeSpace(ODSNameSpaceXML.XFORM)
            addChildTag(Tag("office:scripts"))
            addChildTag(officeStyle)
            addChildTag(fontFace)
            addChildTag(officeBody)
        }
        return XML + tag.getTagString()
    }

    fun writeContentXMLFile(fileName:String): Boolean {
        val dir = context.getExternalFilesDir("ODS${File.separator}$fileName${File.separator}component")
        val fileOut = FileOutputStream(File(dir, "content.xml"))
        val isSuccess = try {
            fileOut.write(getContentXMLString().toByteArray())
            fileOut.close()
            true
        } catch (e: Exception) {
            Log.d("ContentXMl", e.toString())
            false
        }
        return isSuccess
    }
}
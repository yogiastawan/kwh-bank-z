package id.yogiastawan.kwhbankz.odscreator

import android.content.Context
import android.util.Log
import id.yogiastawan.kwhbankz.odscreator.tag.Tag
import java.io.File
import java.io.FileOutputStream

class ManifestRDF(val context: Context) {
    private val tagRootName = "rdf:RDF"
    private val tagChildName = "rdf:Description"
    private val childTag = mutableListOf<Tag>()

    fun getManifestRDFString(): String {
        val rootTag = Tag(tagRootName).apply {
            addXMLNAmeSpace(ODSNameSpaceXML.RDF)
        }

        for (i: Int in 0 until (3)) {
            childTag.add(Tag(tagChildName))
            rootTag.addChildTag(childTag[i])
        }
        childTag[0].apply {
            addPropertyTag("rdf:about", "content.xml")
            val tag = Tag("rdf:type")
            tag.addPropertyTag(
                "rdf:resource",
                "http://docs.oasis-open.org/ns/office/1.2/meta/odf#ContentFile"
            )
            addChildTag(tag)
        }
        childTag[1].apply {
            addPropertyTag("rdf:about", "")
            val tag = Tag("ns0:hasPart")
            tag.addPropertyTag("rdf:resource", "content.xml")
            tag.addXMLNAmeSpace(ODSNameSpaceXML.NSO)
            addChildTag(tag)
        }
        childTag[2].apply {
            addPropertyTag("rdf:about", "")
            val tag = Tag("rdf:type")
            tag.addPropertyTag(
                "rdf:resource",
                "http://docs.oasis-open.org/ns/office/1.2/meta/pkg#Document"
            )
            addChildTag(tag)
        }
        return XML + rootTag.getTagString()
    }

    fun writeManifestRDFFile(fileName:String):Boolean {
        val dir=context.getExternalFilesDir("ODS${File.separator}$fileName${File.separator}component")
        val fileOut= FileOutputStream(File(dir,"manifest.rdf"))
        val isSuccess=try {
            fileOut.write(getManifestRDFString().toByteArray())
            fileOut.close()
            true
        }catch (e:Exception){
            Log.d("ManifestRDF", e.toString())
            false
        }
        return isSuccess
    }
}
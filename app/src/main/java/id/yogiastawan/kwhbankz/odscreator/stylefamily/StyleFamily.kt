package id.yogiastawan.kwhbankz.odscreator.stylefamily

enum class StyleFamily(val styleFamily:String) {
    TABLE("table"),
    TABLE_COLUMN("table-column"),
    TABLE_ROW("table-row"),
    TABLE_CELL("table-cell")
}
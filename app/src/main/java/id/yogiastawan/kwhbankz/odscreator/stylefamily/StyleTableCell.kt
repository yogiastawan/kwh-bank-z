package id.yogiastawan.kwhbankz.odscreator.stylefamily

import id.yogiastawan.kwhbankz.odscreator.stylefamily.aligntext.VerticalAlign
import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class StyleTableCell(private var verticalAlign: VerticalAlign):Tag("style:table-cell-properties") {


    override fun getTagString(): String {
        addPropertyTag("style:text-align-source","fix")
        addPropertyTag("style:repeat-content","false")
        addPropertyTag("style:vertical-align",verticalAlign.verticalAlign)
        return super.getTagString()
    }

    fun setVerticalAlign(verticalAlign: VerticalAlign){
        this.verticalAlign=verticalAlign
    }
    fun getVerticalAlign():VerticalAlign{
        return verticalAlign
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun addChildTag(tag: Tag) {
        super.addChildTag(tag)
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun setChildTag(tag: Tag) {
        super.setChildTag(tag)
    }

}

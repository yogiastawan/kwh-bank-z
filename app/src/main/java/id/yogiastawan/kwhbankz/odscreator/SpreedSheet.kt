package id.yogiastawan.kwhbankz.odscreator

import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class SpreedSheet :Tag("office:spreadsheet"){

    private val table= mutableListOf<Table>()

    fun addTable(table: Table){
        this.table.add(table)
    }

    override fun getTagString(): String {
        val tagTableCalculationSettings=Tag("table:calculation-settings").apply {
            addPropertyTag("table:automatic-find-labels","false")
            addPropertyTag("table:use-regular-expressions","false")
            addPropertyTag("table:use-wildcards","true")
        }
        addChildTag(tagTableCalculationSettings)
        for (i in table){
            addChildTag(i)
        }
        addChildTag(Tag("table:named-expressions"))
        return super.getTagString()
    }

    fun getTables():List<Table>{
        return table
    }
}
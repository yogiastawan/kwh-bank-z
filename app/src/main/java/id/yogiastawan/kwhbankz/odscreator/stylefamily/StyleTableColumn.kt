package id.yogiastawan.kwhbankz.odscreator.stylefamily

import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class StyleTableColumn(private var columnWidth:Float):Tag("style:table-column-properties") {
    override fun getTagString(): String {
        addPropertyTag("fo:break-before","auto")
        addPropertyTag("style:column-width","${columnWidth}in")
        return super.getTagString()
    }

    fun setColumnWidth(columnWidth: Float){
        this.columnWidth=columnWidth
    }

    fun getColumnWidth():Float{
        return columnWidth
    }
    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun addChildTag(tag: Tag) {
        super.addChildTag(tag)
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun setChildTag(tag: Tag) {
        super.setChildTag(tag)
    }
}

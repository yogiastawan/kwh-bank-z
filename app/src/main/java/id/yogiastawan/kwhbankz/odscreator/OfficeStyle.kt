package id.yogiastawan.kwhbankz.odscreator

import id.yogiastawan.kwhbankz.odscreator.stylefamily.*
import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class OfficeStyle(): Tag("office:automatic-styles") {

    fun addStyle(style: Style){
        tags.add(style)
    }

    fun getStyle():List<Style>{
        return tags.map { it as Style }
    }

    override fun getTagString(): String {

        return super.getTagString()
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun setChildTag(tag: Tag) {
        super.setChildTag(tag)
    }


}
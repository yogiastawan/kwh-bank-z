package id.yogiastawan.kwhbankz.odscreator.stylefamily

import id.yogiastawan.kwhbankz.odscreator.stylefamily.aligntext.TextAlign
import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class StyleParagraph(private var textAlign: TextAlign, private var marginLeft:Float):Tag("style:paragraph-properties") {

    override fun getTagString(): String {
        addPropertyTag("fo:text-align",textAlign.textAlign)
        addPropertyTag("fo:margin-left","${marginLeft}in")
        return super.getTagString()
    }

    fun setTextAlign(textAlign: TextAlign){
        this.textAlign=textAlign
    }
    fun getTextAlign():TextAlign{
        return textAlign
    }

    fun setMarginLeft(marginLeft: Float){
        this.marginLeft=marginLeft
    }
    fun getMarginLeft():Float{
        return marginLeft
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun addChildTag(tag: Tag) {
        super.addChildTag(tag)
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun setChildTag(tag: Tag) {
        super.setChildTag(tag)
    }
}
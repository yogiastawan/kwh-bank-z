package id.yogiastawan.kwhbankz.odscreator.celltype

enum class OfficeType(val type:String) {
    STRING("string"),
    FLOAT("float")
}
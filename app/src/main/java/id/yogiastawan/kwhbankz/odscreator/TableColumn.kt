package id.yogiastawan.kwhbankz.odscreator

import id.yogiastawan.kwhbankz.odscreator.stylefamily.Style
import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class TableColumn(private var style: Style, private var numberColumnRepeatedStyle: Int?) :
    Tag("table:table-column") {

    override fun getTagString(): String {
        addPropertyTag("table:style-name", style.getStyleName())
        addPropertyTag("table:default-cell-style-name", "Default")
        if (numberColumnRepeatedStyle != null) {
            addPropertyTag(
                "table:number-columns-repeated",
                numberColumnRepeatedStyle.toString()
            )
        }
        return super.getTagString()
    }

    fun setStyle(style: Style) {
        this.style = style
    }

    fun getStyleString(): Style {
        return style
    }

    fun setNumberColumnWillAffectByStyle(number: Int?) {
        this.numberColumnRepeatedStyle = number
    }

    fun getNumberColumnWillAffectByStyle(): Int? {
        return numberColumnRepeatedStyle
    }

    @Deprecated(
        "Cannot insert child tag in TableColumn", level = DeprecationLevel.HIDDEN
    )
    override fun addChildTag(tag: Tag) {
        super.addChildTag(tag)
    }

}

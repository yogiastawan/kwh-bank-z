package id.yogiastawan.kwhbankz.odscreator

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.util.Log
import id.yogiastawan.kwhbankz.odscreator.tag.Tag
import java.io.File
import java.io.FileOutputStream

class MetaXML(private var nameCreator: String, private val context: Context) {
    private val tagRootName = "office:document-meta"
    private val tagMeta = "office:meta"
    private val tagMetaItem = "meta:initial-creator"
    fun setCreator(nameCreator: String) {
        this.nameCreator = nameCreator
    }

    fun getCreator(): String {
        return nameCreator
    }

    fun getMetaXMLString(): String {
        val tagCreator = Tag(tagMetaItem).apply {
            setValueTag(nameCreator)
        }

        val tagMeta = Tag(tagMeta).apply {
            addChildTag(tagCreator)
        }

        val tagRoot = Tag(tagRootName).apply {
            addXMLNAmeSpace(ODSNameSpaceXML.GRDDL)
            addXMLNAmeSpace(ODSNameSpaceXML.META)
            addXMLNAmeSpace(ODSNameSpaceXML.DC)
            addXMLNAmeSpace(ODSNameSpaceXML.XLINK)
            addXMLNAmeSpace(ODSNameSpaceXML.OOO)
            addXMLNAmeSpace(ODSNameSpaceXML.OFFICE)
            addPropertyTag("office:version", "1.3")
            addChildTag(tagMeta)
        }
        return XML + tagRoot.getTagString()
    }

    fun writeMetaXMLFile(fileName:String): Boolean {
        val dir = context.getExternalFilesDir("ODS${File.separator}$fileName${File.separator}component")
        val fileOut = FileOutputStream(File(dir, "meta.xml"))
        val isSuccess = try {
            fileOut.write(getMetaXMLString().toByteArray())
            fileOut.close()
            true
        } catch (e: Exception) {
            Log.d("MetaXML", e.toString())
            false
        }
        return isSuccess
    }
}
package id.yogiastawan.kwhbankz.odscreator

import id.yogiastawan.kwhbankz.odscreator.fontface.StyleFont
import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class FontFace :Tag("office:font-face-decls"){
    private val fontFace= mutableListOf<StyleFont>()

    override fun getTagString(): String {
        for (i in fontFace){
            addChildTag(i)
        }
        return super.getTagString()
    }


    fun addStyleFont(styleFont: StyleFont){
        fontFace.add(styleFont)
    }

    fun getStyleFont():List<StyleFont>{
        return fontFace
    }
}
package id.yogiastawan.kwhbankz.odscreator.stylefamily.aligntext

enum class VerticalAlign(val verticalAlign: String) {
    TOP("top"),
    MIDDLE("middle"),
    BOTTOM("bottom")
}
package id.yogiastawan.kwhbankz.odscreator.fontface

import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class StyleFont(private var styleName:String, private var fontFamilyGeneric:String):Tag("style:font-face ") {
    override fun getTagString(): String {
        addPropertyTag("style:name", styleName)
        addPropertyTag("svg:font-family","&apos;$styleName&apos;")
        addPropertyTag("style:font-family-generic", fontFamilyGeneric)
        addPropertyTag("style:font-pitch","variable")
        return super.getTagString()
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun addChildTag(tag: Tag) {
        super.addChildTag(tag)
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun setChildTag(tag: Tag) {
        super.setChildTag(tag)
    }
}

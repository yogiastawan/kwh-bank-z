package id.yogiastawan.kwhbankz.odscreator

import id.yogiastawan.kwhbankz.odscreator.stylefamily.Style
import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class Table(private var nameTableProperty: String, private var styleTable: Style): Tag("table:table") {
   private val tag = "table:table"

    private val tableColumn= mutableListOf<TableColumn>()
    private val tableRow= mutableListOf<TableRow>()

    fun addTableRow(tableRow:TableRow){
        tags.add(tableRow)
    }

    fun addTableColumn(tableColumn: TableColumn){
        tags.add(tableColumn)
    }

    override fun getTagString(): String {
        addPropertyTag("table:name", nameTableProperty)
        addPropertyTag("table:style-name", styleTable.getStyleName())
        for (i in tableColumn){
            addChildTag(i)
        }
        for (i in tableRow){
            addChildTag(i)
        }
        return super.getTagString()
    }

    fun setNameTable(nameTable:String){
        nameTableProperty=nameTable
    }

    fun getTableName():String{
        return nameTableProperty
    }

    fun setStyleTable(styleTable: Style){
        this.styleTable=styleTable
    }

    fun getStyleTable(): Style {
        return styleTable
    }
    fun getTableRows():List<TableRow>{
        return tableRow
    }

    fun getTableColumns():List<TableColumn>{
        return tableColumn
    }
}
package id.yogiastawan.kwhbankz.odscreator.stylefamily

import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class StyleTable(private val display: Boolean, private var writingMode: WritingMode) : Tag("style:table-properties"){

    override fun getTagString(): String {
        addPropertyTag("table:display",display.toString())
        addPropertyTag("style:writing-mode",writingMode.writingMode)
        return super.getTagString()
    }

    fun setWritingMode(writingMode: WritingMode){
        this.writingMode=writingMode
    }

    fun getWritingMode():WritingMode{
        return writingMode
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun addChildTag(tag: Tag) {
        super.addChildTag(tag)
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun setChildTag(tag: Tag) {
        super.setChildTag(tag)
    }
}

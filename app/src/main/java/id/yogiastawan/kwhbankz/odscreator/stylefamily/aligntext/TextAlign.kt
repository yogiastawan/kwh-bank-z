package id.yogiastawan.kwhbankz.odscreator.stylefamily.aligntext

enum class TextAlign(val textAlign: String) {
    LEFT("left"),
    CENTER("center"),
    RIGHT("right"),
    JUSTIFIED("justified")
}
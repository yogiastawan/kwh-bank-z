package id.yogiastawan.kwhbankz.odscreator.stylefamily

import id.yogiastawan.kwhbankz.odscreator.TableColumn
import id.yogiastawan.kwhbankz.odscreator.TableRow
import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class Style(private var styleName: String, private var styleFamily: StyleFamily) :
    Tag("style:style") {
    private var styleTable: StyleTable?=null
    fun setStyleTable(styleTable: StyleTable?) {
        this.styleTable = styleTable
    }

    private var styleTableColumn: StyleTableColumn?=null
    fun setStyleTableColumn(styleTableColumn: StyleTableColumn?) {
        this.styleTableColumn = styleTableColumn
    }

    private var styleTableRow: StyleTableRow?=null

    fun setStyleTableRow(styleTableRow: StyleTableRow?) {
        this.styleTableRow = styleTableRow
    }

    private var styleTableCell: StyleTableCell?=null
    fun setStyleTableCell(styleTableCell: StyleTableCell?) {
        this.styleTableCell = styleTableCell
    }

    private var styleParagraph: StyleParagraph?=null
    fun setStyleParagraph(styleParagraph: StyleParagraph?) {
        this.styleParagraph = styleParagraph
    }


    override fun getTagString(): String {
        addPropertyTag("style:name", styleName)
        addPropertyTag("style:family", styleFamily.styleFamily)
        when (styleFamily) {
            StyleFamily.TABLE -> {
                styleTable?.let { setChildTag(it) }
            }
            StyleFamily.TABLE_COLUMN -> {
                styleTableColumn?.let { setChildTag(it) }
            }
            StyleFamily.TABLE_ROW -> {
                styleTableRow?.let { setChildTag(it) }
            }
            StyleFamily.TABLE_CELL -> {
                styleTableCell?.let { setChildTag(it) }
                styleParagraph?.let { setChildTag(it) }
            }
        }
        return super.getTagString()
    }

    fun getStyleName(): String {
        return styleName
    }

    fun setStyleName(styleName: String) {
        this.styleName = styleName
    }

    fun setStyleFamily(styleFamily: StyleFamily) {
        this.styleFamily = styleFamily
    }

    fun getStyleFamily(): StyleFamily {
        return styleFamily
    }

    @Deprecated(
        "Cannot insert child tag in TextTag", level = DeprecationLevel.HIDDEN,
        replaceWith = ReplaceWith(
            "setChildTag(tag:Tag)",
            "import id.yogiastawan.kwhbankz.odscreator.tag.Tag"
        )
    )
    override fun addChildTag(tag: Tag) {
        super.addChildTag(tag)
    }
}
package id.yogiastawan.kwhbankz.odscreator

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.core.net.toFile
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.security.KeyStore
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class ODSCreator(
    private val context: Context
) {

    fun createODS(fileUri: Uri) {
        val outPutStream = context.contentResolver.openOutputStream(fileUri)
        val zipOut = ZipOutputStream(outPutStream)

        //get List File
        val files = context.getExternalFilesDir("ODS${File.separator}${File("${fileUri.path}").name}${File.separator}component")!!.listFiles()

//        Log.d("ODS", files.toString())

        if (files != null) {
            for (file in files) {
                Log.d("ODS", file.name)
                if (file.isDirectory) {//META_INF only contain manifest.xml
//                    Log.d("ODS", file.name+" is directory")
                    val entry = ZipEntry(file.name + File.separator)
                    entry.isDirectory
                    zipOut.putNextEntry(entry)
                    val dir=context.getExternalFilesDir("ODS${File.separator}${File("${fileUri.path}").name}${File.separator}component")
                    val subFiles = File(dir, file.name).listFiles()
                    if (subFiles != null) {
                        for (subFile in subFiles) {
//                            Log.d("ODS-SUB", subFile.name)
                            val subFileInput = BufferedInputStream(FileInputStream(subFile))
                            zipOut.putNextEntry(ZipEntry(file.name+File.separator+subFile.name))
                            subFileInput.copyTo(zipOut, 1024)
                            subFileInput.close()
                            zipOut.closeEntry()
                        }
                    }
                } else {
                    val input = BufferedInputStream(FileInputStream(file))
                    zipOut.putNextEntry(ZipEntry(file.name))
                    input.copyTo(zipOut, 1024)
                    input.close()
                    zipOut.closeEntry()
                }
            }
            zipOut.close()
        }
    }

    fun createODSTemp(name:String){
        val filePath=context.getExternalFilesDir("ODS")
        val outputStream=FileOutputStream(File(filePath, "$name${File.separator}[$name]-log sheet.ods"))
        val zipOut = ZipOutputStream(outputStream)

        val files = File(filePath, "$name${File.separator}component").listFiles()

        if (files!=null){

            for (file in files){
                if (file.isDirectory) {//META_INF only contain manifest.xml
                    val entry = ZipEntry(file.name + File.separator)
                    entry.isDirectory
                    zipOut.putNextEntry(entry)

                    val subFiles = File(
                        filePath,
                        "$name${File.separator}component${File.separator}${file.name}"
                    ).listFiles()
                    if (subFiles != null) {
                        for (subFile in subFiles) {
                            val subFileInput = BufferedInputStream(FileInputStream(subFile))
                            zipOut.putNextEntry(ZipEntry(file.name+File.separator+subFile.name))
                            subFileInput.copyTo(zipOut, 1024)
                            subFileInput.close()
                            zipOut.closeEntry()
                        }
                    }
                } else {
                    val input = BufferedInputStream(FileInputStream(file))
                    zipOut.putNextEntry(ZipEntry(file.name))
                    input.copyTo(zipOut, 1024)
                    input.close()
                    zipOut.closeEntry()
                }
            }
            zipOut.close()

        }
    }


}
package id.yogiastawan.kwhbankz.odscreator

import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class TextTag(private var text: String) : Tag("text:p") {

    override fun getTagString(): String {
        setValueTag(text)
        return super.getTagString()
    }

    fun setText(text: String) {
        this.text = text
    }

    fun getText(): String {
        return text
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun addChildTag(tag: Tag) {
        super.addChildTag(tag)
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun setChildTag(tag: Tag) {
        super.setChildTag(tag)
    }
}

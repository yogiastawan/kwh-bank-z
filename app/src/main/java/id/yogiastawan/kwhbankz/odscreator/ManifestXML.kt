package id.yogiastawan.kwhbankz.odscreator

import android.content.Context
import android.util.Log
import id.yogiastawan.kwhbankz.odscreator.tag.Tag
import java.io.File
import java.io.FileOutputStream

class ManifestXML(val context: Context) {
    private val rootTagName = "manifest:manifest"
    private val childTagName = "manifest:file-entry"

    fun getManifestXMLString(): String {
        val rootTag = Tag(rootTagName).apply {
            addXMLNAmeSpace(ODSNameSpaceXML.MANIFEST)
            addXMLNAmeSpace(ODSNameSpaceXML.LOEXT)
            addPropertyTag("manifest:version", "1.3")
        }

        for (i: Int in 0 until (4)) {
            rootTag.addChildTag(Tag(childTagName))
        }
        rootTag.getTagsChild()[0].apply {
            addPropertyTag("manifest:full-path", "/")
            addPropertyTag("manifest:version", "1.3")
            addPropertyTag("manifest:media-type", "application/vnd.oasis.opendocument.spreadsheet")
        }
        rootTag.getTagsChild()[1].apply {
            addPropertyTag("manifest:full-path", "meta.xml")
            addPropertyTag("manifest:media-type", "text/xml")
        }
        rootTag.getTagsChild()[2].apply {
            addPropertyTag("manifest:full-path", "manifest.rdf")
            addPropertyTag("manifest:media-type", "application/rdf+xml")
        }
        rootTag.getTagsChild()[3].apply {
            addPropertyTag("manifest:full-path", "content.xml")
            addPropertyTag("manifest:media-type", "text/xml")
        }
        return XML + rootTag.getTagString()
    }

    fun writeManifestXML(fileName:String): Boolean {
        val dir=context.getExternalFilesDir("ODS${ File.separator }$fileName${File.separator}component${File.separator}META-INF")
        val fileOut= FileOutputStream(File(dir,"manifest.xml"))
        val isSuccess=try {
            fileOut.write(getManifestXMLString().toByteArray())
            fileOut.close()
            true
        }catch (e:Exception){
            Log.d("ManifestXML>", e.toString())
            false
        }
        return isSuccess
    }
}
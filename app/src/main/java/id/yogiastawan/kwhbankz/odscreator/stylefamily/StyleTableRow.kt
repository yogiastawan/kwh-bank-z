package id.yogiastawan.kwhbankz.odscreator.stylefamily

import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class StyleTableRow(private var rowHeight:Float, private var optimalRowHeight:Boolean):Tag("style:table-row-properties") {
    override fun getTagString(): String {
        addPropertyTag("style:row-height","${rowHeight}in")
        addPropertyTag("fo:break-before","auto")
        addPropertyTag("style:use-optimal-row-height",optimalRowHeight.toString())
        return super.getTagString()
    }

    fun setRowHeight(height:Float){
        rowHeight=height
    }

    fun getRowHeight():Float{
        return rowHeight
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun addChildTag(tag: Tag) {
        super.addChildTag(tag)
    }

    @Deprecated("Cannot add child tag", level = DeprecationLevel.HIDDEN)
    override fun setChildTag(tag: Tag) {
        super.setChildTag(tag)
    }

}

package id.yogiastawan.kwhbankz.odscreator

import id.yogiastawan.kwhbankz.odscreator.celltype.CalCextType
import id.yogiastawan.kwhbankz.odscreator.celltype.OfficeType
import id.yogiastawan.kwhbankz.odscreator.stylefamily.Style
import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class TableCell(
    private var isCovered: Boolean,
    private var officeType: OfficeType,
    private var calCextType: CalCextType,
    private var officeValue: Float?,
    private var styleTableCell: Style?,
    private var columnSpanned: Int?,
    private var rowSpanned: Int?
) : Tag("table:table-cell") {
    private lateinit var textTag: TextTag

    fun setTextTag(textTag: TextTag) {
        this.textTag = textTag
    }

    fun getTextTag(): TextTag {
        return textTag
    }

    override fun getTagString(): String {
        if (isCovered) {
            setTagName("table:covered-table-cell")
        } else {
            addPropertyTag("office:value-type", officeType.type)
            addPropertyTag("calcext:value-type", calCextType.type)

            if (officeValue != null) {
                addPropertyTag("office:value", officeValue.toString())
            }

            if (styleTableCell != null) {
                addPropertyTag("table:style-name", styleTableCell!!.getStyleName())
            }
            if (columnSpanned != null) {
                addPropertyTag("table:number-columns-spanned", columnSpanned.toString())
            }
            if (rowSpanned != null) {
                addPropertyTag("table:number-rows-spanned", rowSpanned.toString())
            }
            addChildTag(textTag)
        }

        return super.getTagString()
    }

    fun setOfficeType(officeType: OfficeType) {
        this.officeType = officeType
    }

    fun getOfficeType(): OfficeType {
        return officeType
    }

    fun setCalCextType(calCextType: CalCextType) {
        this.calCextType = calCextType
    }

    fun getCalCextType(): CalCextType {
        return calCextType
    }

    fun setOfficeValue(officeValue: Float?) {
        this.officeValue = officeValue
    }

    fun getOfficeValue(): Float? {
        return officeValue
    }
}

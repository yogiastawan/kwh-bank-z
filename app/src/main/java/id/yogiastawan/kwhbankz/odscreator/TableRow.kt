package id.yogiastawan.kwhbankz.odscreator

import id.yogiastawan.kwhbankz.odscreator.stylefamily.Style
import id.yogiastawan.kwhbankz.odscreator.tag.Tag

class TableRow(private var styleTableRow: Style) : Tag("table:table-row") {
    private val tableCell= mutableListOf<TableCell>()
    override fun getTagString(): String {
        addPropertyTag("table:style-name", styleTableRow.getStyleName())
        for (i in tableCell){
            addChildTag(i)
        }
        return super.getTagString()
    }

    fun addTableCell(tableCell: TableCell) {
        this.tableCell.add(tableCell)
    }

    fun getTableCell(): List<TableCell> {
        return tableCell
    }

    fun setStyleTableRow(styleTableRow: Style) {
        this.styleTableRow=styleTableRow
    }

    fun getStyleTableRow(): Style {
        return styleTableRow
    }
}
package id.yogiastawan.kwhbankz.dialog

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import id.yogiastawan.kwhbankz.R
import id.yogiastawan.kwhbankz.database.DataBaseHelper
import id.yogiastawan.kwhbankz.database.LOCATION_TABLE
import id.yogiastawan.kwhbankz.listrecycle.DataKwh
import id.yogiastawan.kwhbankz.viewmodel.SharedData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class EditTenantName : DialogFragment() {
    private val TAG = "TENANT DIALOG"
    private lateinit var dataSharedModel: SharedData
    private var dateID: Int = -2

    companion object {
        const val EDIT_TENANT_DIALOG_TAG = "TENANT_DIALOG"
        fun newInstance(
            title: String,
            dataKwh: DataKwh,
            groupIndex: Int,
            positionItem: Int
        ): EditTenantName {
            val dialog = EditTenantName()
            val args = Bundle()
            args.putString("TITLE", title)
            args.putInt("POS_ID", groupIndex)
            args.putInt("POS_ITEM", positionItem)
            args.putParcelable("DATA_KWH", dataKwh)
            dialog.arguments = args
            return dialog
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.edit_tenant_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val name: AppCompatTextView = view.findViewById(R.id.name_tenant)
        val tenantName = requireArguments().getParcelable<DataKwh>("DATA_KWH")!!.name
        name.text = tenantName
        val kwhValue: TextInputEditText = view.findViewById(R.id.edit_name)
        kwhValue.setText(tenantName)
        kwhValue.requestFocus()
        Log.d(TAG, "Edit tenant dialog view created")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view: View? = onCreateView(requireActivity().layoutInflater, null, null)
        onViewCreated(view!!, null)
        dataSharedModel = ViewModelProvider(requireActivity()).get(SharedData::class.java)
        val db = DataBaseHelper(requireContext())
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())

        val posId = requireArguments().getInt("POS_ID")
        val posItem = requireArguments().getInt("POS_ITEM")
        val oldDataKwh = requireArguments().getParcelable<DataKwh>("DATA_KWH")
        val oldName = oldDataKwh!!.name
        val editText = view.findViewById<TextInputEditText>(R.id.edit_name)
        val textInputLayout = view.findViewById<TextInputLayout>(R.id.text_input_layout_tenant)

        dialogBuilder.apply {
            setTitle(requireArguments().getString("TITLE"))
            setPositiveButton(
                resources.getString(R.string.update)
            ) { _, _ ->
                CoroutineScope(Dispatchers.Main).launch {
                    val numberTenantChanged = async(Dispatchers.IO) {
                        db.updateTenantNameAtId(
                            oldDataKwh.tenantId!!,
                            editText.text.toString(),
                            posId
                        )
                    }
                    if (numberTenantChanged.await() > 0) {
                        dataSharedModel.editDataNameItem(posId, editText.text.toString(), posItem)
                        Log.d(
                            TAG,
                            "Changed $oldName to ${editText.text.toString()} on id: ${oldDataKwh.tenantId}"
                        )
                    }
                }
            }
            setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
        }
        dialogBuilder.setView(view)
        val dialog = dialogBuilder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
        }
        editText.doOnTextChanged { text, _, _, _ ->
            CoroutineScope(Dispatchers.Main).launch {
                //            Log.d(TAG, "$oldName || $text")
                if (text!!.isEmpty()) {
                    textInputLayout.error = resources.getString(R.string.name_is_empty)
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
                } else {
                    val isTenantExist = async(Dispatchers.IO) {
                        db.isTenantAlreadyExist(posId, text.toString())
                    }
//                Log.d(TAG, (oldName==text.toString()).toString())
                    if (isTenantExist.await() && oldName != text.toString()) {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
                        textInputLayout.error = resources.getString(R.string.error_tenant_exist)+" ${LOCATION_TABLE[posId]}*"
                    } else if (isTenantExist.await() && oldName == text.toString()) {
                        textInputLayout.error = null
                        textInputLayout.helperText = resources.getString(R.string.input_tenant_name)
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false

                    } else {
                        textInputLayout.error = null
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = true
                        textInputLayout.helperText =
                            resources.getString(R.string.tenant_name_will_change) + " $text"
                    }
                }
            }
        }
        dialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }
}
package id.yogiastawan.kwhbankz.dialog

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import id.yogiastawan.kwhbankz.R
import id.yogiastawan.kwhbankz.database.DataBaseHelper
import id.yogiastawan.kwhbankz.listrecycle.DataKwh
import id.yogiastawan.kwhbankz.viewmodel.SharedData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class EditDialog : DialogFragment() {

    private val TAG="EDIT DIALOG"
    private lateinit var dataSharedModel: SharedData
    private var dateID: Int = -2

    companion object {
        const val EDIT_DIALOG_TAG = "EDIT_DIALOG"
        fun newInstance(
            title: String,
            dataKwh: DataKwh,
            groupIndex: Int,
            positionItem: Int
        ): EditDialog {
            val dialog = EditDialog()
            val args = Bundle()
            args.putString("TITLE", title)
            args.putInt("POS_ID", groupIndex)
            args.putInt("POS_ITEM", positionItem)
            args.putParcelable("DATA_KWH", dataKwh)
            dialog.arguments = args
            return dialog
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.edit_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val name: AppCompatTextView = view.findViewById(R.id.name)
        name.text = requireArguments().getParcelable<DataKwh>("DATA_KWH")!!.name
        val kwhValue: TextInputEditText = view.findViewById(R.id.edit_value)
        val kwh: Float? = requireArguments().getParcelable<DataKwh>("DATA_KWH")!!.kwh
        if (kwh !=null) {
            kwhValue.setText("$kwh")
        }
        kwhValue.requestFocus()
        Log.d(TAG, "edit dialog view created: $kwh")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view: View? = onCreateView(requireActivity().layoutInflater, null, null)
        onViewCreated(view!!, null)
        dataSharedModel = ViewModelProvider(requireActivity()).get(SharedData::class.java)
        val db = DataBaseHelper(requireContext())
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())

        val posId = requireArguments().getInt("POS_ID")
        val posItem = requireArguments().getInt("POS_ITEM")
        val oldDataKwh = requireArguments().getParcelable<DataKwh>("DATA_KWH")

        val editText = view.findViewById<TextInputEditText>(R.id.edit_value)
        val textInputLayout=view.findViewById<TextInputLayout>(R.id.text_input_layout)

        dialogBuilder.apply {
            setTitle(requireArguments().getString("TITLE"))
            setPositiveButton(if(oldDataKwh!!.kwhValueId==null)resources.getString(R.string.save) else resources.getString(R.string.update)) { _, _ ->
                CoroutineScope(Dispatchers.Main).launch {
                    val newKwhValue = editText.text.toString().toFloat()
                    if (oldDataKwh.kwhValueId==null) {
                        val kwhId = async(Dispatchers.IO) {
                            db.insertKwhValue(
                                posId,
                                oldDataKwh.tenantId!!,
                                dateID,
                                newKwhValue
                            )
                        }
                        val id = kwhId.await()
                        if (id > 0) {
                            dataSharedModel.editDataValueItem(
                                posId,
                                newKwhValue,
                                id,
                                posItem
                            )
                        }
                    }else{
                        val numbKwhUpdate=async(Dispatchers.IO){
                            db.updateKwhValue(oldDataKwh.kwhValueId!!.toLong(),newKwhValue,posId)
                        }
                        if (numbKwhUpdate.await()>0){
                            Log.d(TAG, "numb kwh changed: ${numbKwhUpdate.await()} with id: ${oldDataKwh.kwhValueId}")
                            dataSharedModel.editDataValueItem(posId,newKwhValue,
                                oldDataKwh.kwhValueId!!, posItem)
                        }
                    }
                }
            }
            setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
        }
        dialogBuilder.setView(view)
        val dialog = dialogBuilder.create()
        dialog.setOnShowListener {
            dataSharedModel.getDateId().observe(viewLifecycleOwner, {
                dateID = it
            })
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled=false
            if (editText.text!!.isEmpty()){
                textInputLayout.error=resources.getString(R.string.data_kwh_empty)
            }
        }
        editText.doOnTextChanged { text, _, _, _ ->
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = text!!.isNotEmpty()
            if (text.isEmpty()){
                textInputLayout.error=resources.getString(R.string.data_kwh_empty)
            }else{
                textInputLayout.error=null
            }
        }
        dialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }
}
package id.yogiastawan.kwhbankz.database

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*


class DateRecorded {
    var id: Int = 0
    var dateRecorded: String = ""
    private val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())

    init {
        dateRecorded=dateFormat.format(Date())
    }

    constructor(_id: Int, _date: Date) {
        id = _id
        dateRecorded = dateFormat.format(_date)
    }

    constructor(_date: Date) {
        dateRecorded = dateFormat.format(_date)
        Log.d("DateRecorded", dateRecorded)
    }

}
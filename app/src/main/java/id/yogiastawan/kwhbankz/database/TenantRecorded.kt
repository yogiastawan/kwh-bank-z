package id.yogiastawan.kwhbankz.database

class TenantRecorded {
    var id:Int=0
    var tenantName:String=""

    constructor(_id: Int, _tenantName:String){
        id=_id
        tenantName=_tenantName
    }

    constructor(_tenantName: String){
        tenantName=_tenantName
    }
}
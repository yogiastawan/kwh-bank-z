package id.yogiastawan.kwhbankz.database

class KwhRecorded {
    var id:Int=0
    var kwhData: Float=0f

    constructor(_id:Int,_kwhData:Float){
        id=_id
        kwhData=_kwhData
    }

    constructor(_kwhData: Float){
        kwhData=_kwhData
    }
}
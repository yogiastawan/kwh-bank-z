package id.yogiastawan.kwhbankz.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DatabaseUtils
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import id.yogiastawan.kwhbankz.listrecycle.DataKwh
import java.text.SimpleDateFormat
import java.util.*

class DataBaseHelper(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    private val TAG = "DBHELPER"

    companion object {
        const val DATABASE_NAME = "kwh.db"
        const val DATABASE_VERSION = 3

        //        val TABLE_NAME: List<String> = listOf("DATE_RECORDED,KWH_VALUE, TENANT_NAME")
        const val TABLE_NAME_KWH_VALUE = "KWH_VALUE"
        const val TABLE_NAME_TENANT_NAME = "TENANT_NAME"
        const val TABLE_NAME_DATE_RECORDED = "DATE_RECORDED"
        const val TABLE_NAME_TAG = "KWH_ID_TAGGED"
        const val KEY_ID = "id"
        const val KEY_ID_TENANT = "id_tenant"
        const val KEY_DATE = "date_recorded"
        const val KEY_TENANT_NAME = "name_tenant"
        const val KEY_TENANT_STATUS="tenant_status"
        const val KEY_ID_KWH_VALUE = "id_kwh_value"
        const val KEY_KWH_VALUE = "kwh_value"
        const val KEY_DATE_ID = "date_recorded_id"
        const val KEY_TENANT_NAME_ID = "name_tenant_id"
        const val KEY_KWH_VALUE_ID = "kwh_value_id"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        //create table
        db!!.execSQL("CREATE TABLE TABLE_${TABLE_NAME_DATE_RECORDED}($KEY_ID INTEGER PRIMARY KEY AUTOINCREMENT, $KEY_DATE CHAR(10) UNIQUE NOT NULL)")
        for (i: Int in 0 until (LOCATION_TABLE.size)) {
            db.execSQL("CREATE TABLE TABLE_${LOCATION_TABLE[i]}_${TABLE_NAME_TENANT_NAME} ($KEY_ID_TENANT INTEGER PRIMARY KEY AUTOINCREMENT, $KEY_TENANT_NAME CHAR(80) UNIQUE NOT NULL, KEY_$KEY_TENANT_STATUS NOT NULL)")
            db.execSQL("CREATE TABLE TABLE_${LOCATION_TABLE[i]}_${TABLE_NAME_KWH_VALUE} ($KEY_ID_KWH_VALUE INTEGER PRIMARY KEY, $KEY_KWH_VALUE FLOAT NOT NULL)")
            db.execSQL("CREATE TABLE TABLE_${LOCATION_TABLE[i]}_${TABLE_NAME_TAG} ($KEY_ID INTEGER PRIMARY KEY, $KEY_DATE_ID INTEGER NOT NULL, $KEY_TENANT_NAME_ID INTEGER NOT NULL, $KEY_KWH_VALUE_ID INTEGER)")
        }
        Log.d(TAG, "Database Create")

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS TABLE_${TABLE_NAME_DATE_RECORDED}")
        for (i: Int in 0 until (LOCATION_TABLE.size)) {
            db.execSQL("DROP TABLE IF EXISTS TABLE_${LOCATION_TABLE[i]}_${TABLE_NAME_TENANT_NAME}")
            db.execSQL("DROP TABLE IF EXISTS TABLE_${LOCATION_TABLE[i]}_${TABLE_NAME_KWH_VALUE}")
            db.execSQL("DROP TABLE IF EXISTS TABLE_${LOCATION_TABLE[i]}_${TABLE_NAME_TAG}")
        }
        Log.d(TAG, "Database Upgrade")
        onCreate(db)
    }

    // Date table
    fun insertDate(date: DateRecorded): Long {
        val db: SQLiteDatabase = this.writableDatabase
        val value = ContentValues()
        value.put(KEY_DATE, date.dateRecorded)
        val numb=db.insertWithOnConflict(
            "TABLE_${TABLE_NAME_DATE_RECORDED}",
            null,
            value,
            SQLiteDatabase.CONFLICT_IGNORE
        )
        db.close()
        return numb
    }

    fun getDateAtId(id: Long, groupIndex: Int): DateRecorded {
        val db: SQLiteDatabase = this.readableDatabase
        var date: Date? = Date()
        val cursor =
            db.rawQuery(
                "SELECT $KEY_DATE FROM TABLE_${TABLE_NAME_DATE_RECORDED} WHERE $KEY_ID='$id'",
                null
            )
        if (cursor.moveToFirst()) {
            date = SimpleDateFormat(
                "yyyy-MM-dd",
                Locale.getDefault()
            ).parse(
                cursor.getString(
                    cursor.getColumnIndex(
                        KEY_DATE
                    )
                )
            )
        }
        cursor.close()
        db.close()
        return DateRecorded(date!!)
    }

    fun getAllDate(groupIndex: Int): List<DateRecorded> {
        val db: SQLiteDatabase = this.readableDatabase
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val date = mutableListOf<DateRecorded>()
        val cursor =
            db.rawQuery(
                "SELECT * FROM TABLE_${TABLE_NAME_DATE_RECORDED}",
                null
            )
        if (cursor.moveToFirst()) {
            do {
                val dateRec = DateRecorded(
                    cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                    dateFormat.parse(
                        cursor.getString(
                            cursor.getColumnIndex(
                                KEY_DATE
                            )
                        )
                    )!!
                )
                date.add(dateRec)
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return date
    }

    fun getDateIdOnDate(date: DateRecorded, groupIndex: Int): Long {
        val db: SQLiteDatabase = this.readableDatabase
        var id: Long = 0
        val cursor =
            db.rawQuery(
                "SELECT $KEY_ID FROM TABLE_${TABLE_NAME_DATE_RECORDED} WHERE $KEY_DATE='${date.dateRecorded}'",
                null
            )
        if (cursor.moveToFirst()) {
            id = cursor.getLong(cursor.getColumnIndex(KEY_ID))
        }
        cursor.close()
        db.close()
        return id
    }
    fun getDateIdOnDateString(date: String, groupIndex: Int): Long {
        val db: SQLiteDatabase = this.readableDatabase
        var id: Long = 0
        val cursor =
            db.rawQuery(
                "SELECT $KEY_ID FROM TABLE_${TABLE_NAME_DATE_RECORDED} WHERE $KEY_DATE='${date}'",
                null
            )
        if (cursor.moveToFirst()) {
            id = cursor.getLong(cursor.getColumnIndex(KEY_ID))
        }
        cursor.close()
        db.close()
        return id
    }

    // tenant Table
    fun insertTenantName(tenantName: TenantRecorded, groupIndex: Int): Long {
        val db: SQLiteDatabase = this.writableDatabase
        val value = ContentValues()
        value.put(KEY_TENANT_NAME, tenantName.tenantName)
        val numb= db.insertWithOnConflict(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TENANT_NAME}",
            null,
            value,
            SQLiteDatabase.CONFLICT_IGNORE
        )
        db.close()
        return numb
    }

    fun insertTenantNameWithString(tenantName: String, groupIndex: Int): Long {
        val db: SQLiteDatabase = this.writableDatabase
        val value = ContentValues()
        value.put(KEY_TENANT_NAME, tenantName)
        val numb= db.insertWithOnConflict(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TENANT_NAME}",
            null,
            value,
            SQLiteDatabase.CONFLICT_IGNORE
        )
        db.close()
        return numb
    }

    fun updateTenantName(oldName: String, newName: String, groupIndex: Int): Int {
        val db: SQLiteDatabase = this.writableDatabase
        val value = ContentValues()
        value.put(KEY_TENANT_NAME, newName)
        val numb= db.updateWithOnConflict(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TENANT_NAME}",
            value,
            "${KEY_TENANT_NAME}=?",
            arrayOf(oldName),
            SQLiteDatabase.CONFLICT_IGNORE
        )
        db.close()
        return numb
    }

    fun updateTenantNameAtId(id: Int, newName: String, groupIndex: Int): Int {
        val db: SQLiteDatabase = this.writableDatabase
        val value = ContentValues()
        value.put(KEY_TENANT_NAME, newName)
        val numb= db.update(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TENANT_NAME}",
            value,
            "${KEY_ID_TENANT}=?",
            arrayOf(id.toString())
        )
        db.close()
        return numb
    }

    private fun deleteTenantName(tenantId: Int, groupIndex: Int): Int {
        val db: SQLiteDatabase = this.writableDatabase
        val numb= db.delete(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TENANT_NAME}",
            "$KEY_ID_TENANT=?",
            arrayOf(tenantId.toString())
        )
        db.close()
        return numb
    }

    fun getAllTenantNamesCursor(groupIndex: Int): Cursor {
        val db: SQLiteDatabase = this.readableDatabase
        val cursor= db.rawQuery(
            "SELECT * FROM TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TENANT_NAME}",
            null
        )
        db.close()
        return cursor
    }

    fun getAllTenantNamesWithId(groupIndex: Int): List<TenantRecorded> {
        val db: SQLiteDatabase = this.readableDatabase
        val tenants = mutableListOf<TenantRecorded>()
        val cursor =
            db.rawQuery(
                "SELECT * FROM TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TENANT_NAME}",
                null
            )
        if (cursor.moveToFirst()) {
            do {
                val tenant = TenantRecorded(
                    cursor.getInt(cursor.getColumnIndex(KEY_ID_TENANT)),
                    cursor.getString(
                        cursor.getColumnIndex(
                            KEY_TENANT_NAME
                        )
                    )
                )
                tenants.add(tenant)
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return tenants
    }

    fun getTenantNameAtId(id: Long, groupIndex: Int): String {
        val db: SQLiteDatabase = this.readableDatabase
        lateinit var name: String
        val cursor =
            db.rawQuery(
                "SELECT $KEY_TENANT_NAME FROM TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TENANT_NAME} WHERE $KEY_ID_TENANT=$id",
                null
            )
        if (cursor.moveToFirst()) {
            name = cursor.getString(cursor.getColumnIndex(KEY_TENANT_NAME))
        }
        cursor.close()
        db.close()
        return name
    }

    fun getTenantIdOnName(name: String, groupIndex: Int): Long {
        val db: SQLiteDatabase = this.readableDatabase
        var id: Long = 0
        val cursor =
            db.rawQuery(
                "SELECT $KEY_ID_TENANT FROM TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TENANT_NAME} WHERE $KEY_TENANT_NAME=$name",
                null
            )
        if (cursor.moveToFirst()) {
            id = cursor.getLong(cursor.getColumnIndex(KEY_ID_TENANT))
        }
        cursor.close()
        db.close()
        return id
    }

    fun isTenantAlreadyExist(groupIndex: Int, name: String): Boolean {
        val db: SQLiteDatabase = this.readableDatabase
        val numbTenantExist = DatabaseUtils.queryNumEntries(
            db,
            "TABLE_${LOCATION_TABLE[groupIndex]}_$TABLE_NAME_TENANT_NAME",
            "$KEY_TENANT_NAME=?",
            arrayOf(name)
        )
        db.close()
        if (numbTenantExist <= 0) {
            return false
        }
        return true
    }

    // Kwh Table
    private fun insertKwhValueFromFloat(kwhValue: Float, groupIndex: Int): Long {
        val db: SQLiteDatabase = this.writableDatabase
        val value = ContentValues()
        value.put(KEY_KWH_VALUE, kwhValue)
        val numb= db.insert(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_KWH_VALUE}",
            null,
            value
        )
        db.close()
        return numb
    }

    fun insertKwhValueFromDataKwh(dataKwh: DataKwh, groupIndex: Int): Long {
        val db: SQLiteDatabase = this.writableDatabase
        val value = ContentValues()
        value.put(KEY_KWH_VALUE, dataKwh.kwh)
        val numb= db.insert(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_KWH_VALUE}",
            null,
            value
        )
        db.close()
        return numb
    }

    fun insertKwhValueFromKwhRecorded(dataKwh: KwhRecorded, groupIndex: Int): Long {
        val db: SQLiteDatabase = this.writableDatabase
        val value = ContentValues()
        value.put(KEY_KWH_VALUE, dataKwh.kwhData)
        val numb =db.insert(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_KWH_VALUE}",
            null,
            value
        )
        db.close()
        return numb
    }

    fun updateKwhValue(id: Long, newValue: Float, groupIndex: Int): Int {
        val db: SQLiteDatabase = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_KWH_VALUE, newValue)
        val numb= db.update(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_KWH_VALUE}",
            values,
            "$KEY_ID_KWH_VALUE=?",
            arrayOf(id.toString())
        )
        db.close()
        return numb
    }

    fun deleteKwhValue(kwhValueId: Int, groupIndex: Int): Int {
        val db: SQLiteDatabase = this.writableDatabase
        val numb= db.delete(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_KWH_VALUE}",
            "$KEY_ID_KWH_VALUE=?",
            arrayOf(kwhValueId.toString())
        )
        db.close()
        return numb
    }

    private fun deleteKwhValues(kwhValueId: List<Int>, groupIndex: Int): Int {
        val db: SQLiteDatabase = this.writableDatabase
        val numb= db.delete(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_KWH_VALUE}",
            "$KEY_ID_KWH_VALUE=?",
            kwhValueId.map { it.toString() }.toTypedArray()
        )
        db.close()
        return numb
    }

    private fun getKwhValueIdAssociatedWithTenantId(groupIndex: Int, tenantId: Int): List<Int> {
        val db: SQLiteDatabase = this.readableDatabase
        val tableKwhValue = "TABLE_${LOCATION_TABLE[groupIndex]}_$TABLE_NAME_KWH_VALUE tableKWH"
        val tableDate = "TABLE_${LOCATION_TABLE[groupIndex]}_$TABLE_NAME_DATE_RECORDED tableDate"
        val tableTenant =
            "TABLE_${LOCATION_TABLE[groupIndex]}_$TABLE_NAME_TENANT_NAME tableTenant"
        val tableTag = "TABLE_${LOCATION_TABLE[groupIndex]}_$TABLE_NAME_TAG tableTag"
        val query =
            "SELECT $KEY_ID_KWH_VALUE FROM " +
                    "$tableKwhValue, $tableTenant, $tableDate, $tableTag WHERE " +
                    "$tableTenant.$KEY_ID=$tenantId AND $tableKwhValue.$KEY_ID_KWH_VALUE=$tableTag.$KEY_KWH_VALUE_ID AND " +
                    "$tableDate.$KEY_ID=$tableTag.$KEY_DATE_ID AND $tableTenant.$KEY_ID_TENANT=$tableTag.$KEY_TENANT_NAME_ID"
        val cursor = db.rawQuery(query, null)
        val list = mutableListOf<Int>()
        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getInt(cursor.getColumnIndex(KEY_ID_KWH_VALUE)))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return list
    }

    fun getKwhValueAtId(id: Long, groupIndex: Int): Float {
        val db: SQLiteDatabase = this.readableDatabase
        val cursor =
            db.rawQuery(
                "SELECT $KEY_KWH_VALUE FROM TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_KWH_VALUE} WHERE ${KEY_ID_KWH_VALUE}=${id}",
                null
            )
        var kwhValue = 0f
        if (cursor.moveToFirst()) {
            kwhValue = cursor.getFloat(cursor.getColumnIndex(KEY_ID_KWH_VALUE))
        }
        cursor.close()
        db.close()
        return kwhValue
    }

    fun getAllKwhValue(groupIndex: Int): List<Float> {
        val db: SQLiteDatabase = this.readableDatabase
        val kwhValues = mutableListOf<Float>()
        val cursor =
            db.rawQuery(
                "SELECT $KEY_KWH_VALUE FROM TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_KWH_VALUE}",
                null
            )
        if (cursor.moveToFirst()) {
            do {
                kwhValues.add(cursor.getFloat(cursor.getColumnIndex(KEY_KWH_VALUE)))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return kwhValues
    }

    //Tagged ID Table
    private fun insertTag(dateId: Int, tenantId: Int, kwhValueId: Int, groupIndex: Int): Long {
        val db: SQLiteDatabase = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_DATE_ID, dateId)
        values.put(KEY_TENANT_NAME_ID, tenantId)
        values.put(KEY_KWH_VALUE_ID, kwhValueId)
        val numb=db.insert("TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TAG}", null, values)
        db.close()
        return numb
    }

    fun deleteTag(tenantId: Int, groupIndex: Int): Int {
        val db: SQLiteDatabase = this.writableDatabase
        val numb= db.delete(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TAG}",
            "$KEY_TENANT_NAME_ID=?",
            arrayOf(tenantId.toString())
        )
        db.close()
        return numb
    }

    private fun deleteTagsAssociatedTenantId(tenantId: List<Int>, groupIndex: Int): Int {
        val db: SQLiteDatabase = this.writableDatabase
        val numb= db.delete(
            "TABLE_${LOCATION_TABLE[groupIndex]}_${TABLE_NAME_TAG}", "$KEY_TENANT_NAME_ID=?",
            tenantId.map { it.toString() }.toTypedArray()
        )
        db.close()
        return numb
    }

    suspend fun getDataKwhList(location: Int, dateId: Int): List<DataKwh> {
        val db: SQLiteDatabase = this.readableDatabase
        val tableKwhValue = "TABLE_${LOCATION_TABLE[location]}_$TABLE_NAME_KWH_VALUE tableKWH"
        val tableDate = "TABLE_${LOCATION_TABLE[location]}_$TABLE_NAME_DATE_RECORDED tableDate"
        val tableTenant = "TABLE_${LOCATION_TABLE[location]}_$TABLE_NAME_TENANT_NAME tableTenant"
        val tableTag = "TABLE_${LOCATION_TABLE[location]}_$TABLE_NAME_TAG tableTag"
        val query =
            "SELECT $KEY_ID_TENANT, $KEY_TENANT_NAME, $KEY_ID_KWH_VALUE, $KEY_KWH_VALUE FROM " +
                    "$tableKwhValue, $tableTenant, $tableDate, $tableTag WHERE " +
                    "$tableDate.$KEY_ID='$dateId' AND $tableKwhValue.$KEY_ID_KWH_VALUE=$tableTag.$KEY_KWH_VALUE_ID AND " +
                    "$tableDate.$KEY_ID=$tableTag.$KEY_DATE_ID AND $tableTenant.$KEY_ID_TENANT=$tableTag.$KEY_TENANT_NAME_ID"
        val cursor = db.rawQuery(query, null)
        val list = mutableListOf<DataKwh>()
        if (cursor.moveToFirst()) {
            do {
                val kwh = DataKwh(
                    cursor.getInt(cursor.getColumnIndex(KEY_ID_TENANT)), cursor.getString(
                        cursor.getColumnIndex(
                            KEY_TENANT_NAME
                        )
                    ), cursor.getInt(cursor.getColumnIndex(KEY_ID_KWH_VALUE)), cursor.getFloat(
                        cursor.getColumnIndex(
                            KEY_KWH_VALUE
                        )
                    )
                )
                list.add(kwh)
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return list
    }

    suspend fun insertKwhValue(
        location: Int,
        tenantId: Int,
        dateId: Int,
        kwhValue: Float
    ): Int {
        val kwhInserted = insertKwhValueFromFloat(kwhValue, location)
        if (kwhInserted <= 0) {
            Log.d(TAG, "cannot insert kwh value")
            return kwhInserted.toInt()
        }
        val tagInserted = insertTag(dateId, tenantId, kwhInserted.toInt(), location)
        if (tagInserted <= 0) {
            Log.d(TAG, "cannot insert tag row")
            return kwhInserted.toInt() + tagInserted.toInt()
        }
        Log.d(
            TAG,
            "insert tag row $kwhValue Success in kwhId: $kwhInserted and in tag: $tagInserted"
        )
        return kwhInserted.toInt()
    }

    suspend fun deleteTenantAndAssociation(tenantId: Int, location: Int): Boolean {
        val tenantDeleted = deleteTenantName(tenantId, location)
        if (tenantDeleted <= 0) {
            Log.d(TAG, "Cannot delete tenant")
            return false
        }
        val kwhValueIds = getKwhValueIdAssociatedWithTenantId(location, tenantId)
        val kwhDeleted = deleteKwhValues(kwhValueIds, location)
        if (kwhDeleted <= 0) {
            Log.d(TAG, "cannot delete kwh")
            return false
        }
        val tagRowDeleted = deleteTagsAssociatedTenantId(kwhValueIds, location)
        if (tagRowDeleted <= 0) {
            Log.d(TAG, "cannot delete tag rows")
            return false
        }
        return true
    }
}